import './App.css';
import axios from 'axios';

let showCount = 0;

const inCre = async () => {
  let result = await axios.post('http://localhost:8080/')
  console.log(result.data)
  showCount = result.data;
}

const getCount = async () => {
  let result = await axios.get('http://localhost:8080/')
  console.log('count = '+ result.data)
  showCount = result.data;
}


function App() {
  return (
    <div className="App">
      {showCount}<br/>
      <button onClick={inCre}>increment</button>
      <button onClick={getCount}>Count</button>
    </div>
  );
}

export default App;
