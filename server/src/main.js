const express = require('express')
const app = express()
const cors = require('cors')
const port = 8080

app.use(cors())

let count = 0;

app.get('/', (req, res) => {
  res.send(count+'')
})

app.post('/',(req, res) => {
    count = count + 1;
    res.send(count+'')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})